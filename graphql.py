from graphqlclient import GraphQLClient
from urllib.error import URLError
import json

# GraphQL API Endpoint
ENDPOINT = "http://localhost:4000/api/graphql"

#####################################################################
# List of lambda functions
# Format GraphQL operation strings

opConnectNode = lambda serialNumber, deviceName: '''
    mutation {{
        connectNode(serialNumber: "{}", deviceName: "{}") {{
            serialNumber
            deviceName
        }}
    }}
'''.format(serialNumber, deviceName)

opDisconnectNode = lambda serialNumber: '''
    mutation {{
        disconnectNode(serialNumber: "{}")
    }}
'''.format(serialNumber)

opUpdateBatteryLevel = lambda serialNumber, value: '''
    mutation {{
        updateBatteryLevel(serialNumber: "{}", value: {})
    }}
'''.format(serialNumber, value)

opLogTemperature = lambda serialNumber, value: '''
    mutation {{
        logTemperature(serialNumber: "{}", value: {})
    }}
'''.format(serialNumber, value)

opLogHumidity = lambda serialNumber, value: '''
    mutation {{
        logHumidity(serialNumber: "{}", value: {})
    }}
'''.format(serialNumber, value)

opLogCO2Level = lambda serialNumber, value: '''
    mutation {{
        logCO2Level(serialNumber: "{}", value: {})
    }}
'''.format(serialNumber, value)

opLogLightIntensity = lambda serialNumber, value: '''
    mutation {{
        logLightIntensity(serialNumber: "{}", value: {})
    }}
'''.format(serialNumber, value)

opLogSoundLevel = lambda serialNumber, value: '''
    mutation {{
        logSoundLevel(serialNumber: "{}", value: {})
    }}
'''.format(serialNumber, value)

opLogMotionDetector = lambda serialNumber, value: '''
    mutation {{
        logMotionDetector(serialNumber: "{}", value: {})
    }}
'''.format(serialNumber, 'true' if value else 'false')

#####################################################################


class API:
    """GraphQL API Client
    """

    def __init__(self):
        """Constructor for API
        """
        self._client = GraphQLClient(ENDPOINT)

    def log(self, serialNumber, msg):
        """API Logger

        Args:
            serialNumber (str): Sensor node serial number
            msg (str): Message to log
        """
        print("GQL> [" + serialNumber + "] - " + msg)

    def _execute(self, serialNumber, op, opName):
        """Execute GraphQL operation

        Args:
            serialNumber (str): Sensor node serial number
            op (str): GraphQL operation string
            opName (str): name of the GraphQL operation

        Returns:
            Dict[str, object]: GraphQL API Response
        """
        try:
            result = json.loads(self._client.execute(op))

            # Prints any error
            if 'errors' in result.keys():
                self.log(serialNumber, "Graphql Operation Failed (" + opName + ")")
                print(str(result['errors']))

            # Return GraphQL API Response
            if 'data' in result.keys():
                return result['data']

        except URLError:
            self.log(serialNumber, "API not reachable")

    def connectNode(self, serialNumber, deviceName):
        """GraphQL Mutation - connectNode

        Args:
            serialNumber (str): Sensor node serial number
            deviceName (str): Sensor node name
        """
        self.log(serialNumber, " Connected to " + deviceName)
        self._execute(serialNumber, opConnectNode(serialNumber, deviceName), 'connectNode')

    def disconnectNode(self, serialNumber):
        """GraphQL Mutation - disconnectNode

        Args:
            serialNumber (str): Sensor node serial number
        """
        self.log(serialNumber, " Disconnected")
        self._execute(serialNumber, opDisconnectNode(serialNumber), 'disconnectNode')

    def updateBatteryLevel(self, serialNumber, value):
        """GraphQL Mutation - updateBatteryLevel

        Args:
            serialNumber (str): Sensor node serial number
            value (int): Battery level value
        """
        self.log(serialNumber, " Update battery: " + str(value) + " %")
        self._execute(serialNumber, opUpdateBatteryLevel(serialNumber, value), 'updateBatteryLevel')

    def logTemperature(self, serialNumber, value):
        """GraphQL Mutation - logTemperature

        Args:
            serialNumber (str): Sensor node serial number
            value (int): Temperature value
        """
        self.log(serialNumber, " Log temperature: " + str(value) + " °C")
        self._execute(serialNumber, opLogTemperature(serialNumber, value), 'logTemperature')

    def logHumidity(self, serialNumber, value):
        """GraphQL Mutation - logHumidity

        Args:
            serialNumber (str): Sensor node serial number
            value (int): Humidity value
        """
        self.log(serialNumber, " Log humidity: " + str(value) + " %")
        self._execute(serialNumber, opLogHumidity(serialNumber, value), 'logHumidity')

    def logCO2Level(self, serialNumber, value):
        """GraphQL Mutation - logCO2Level

        Args:
            serialNumber (str): Sensor node serial number
            value (int): CO2 level value
        """
        self.log(serialNumber, " Log CO2 level: " + str(value) + " PPM")
        self._execute(serialNumber, opLogCO2Level(serialNumber, value), 'logCO2Level')

    def logLightIntensity(self, serialNumber, value):
        """GraphQL Mutation - logLightIntensity

        Args:
            serialNumber (str): Sensor node serial number
            value (int): Light intensity value
        """
        self.log(serialNumber, " Log light intensity: " + str(value) + " Lux")
        self._execute(serialNumber, opLogLightIntensity(serialNumber, value), 'logLightIntensity')

    def logSoundLevel(self, serialNumber, value):
        """GraphQL Mutation - logSoundLevel

        Args:
            serialNumber (str): Sensor node serial number
            value (int): Sound level value
        """
        self.log(serialNumber, " Log sound level: " + str(value) + " dB")
        self._execute(serialNumber, opLogSoundLevel(serialNumber, value), 'logSoundLevel')

    def logMotionDetector(self, serialNumber, value):
        """GraphQL Mutation - logMotionDetector

        Args:
            serialNumber (str): Sensor node serial number
            value (bool): Motion detector value
        """
        self.log(serialNumber, " Log motion detector: " + str(value))
        self._execute(serialNumber, opLogMotionDetector(serialNumber, value), 'logMotionDetector')
