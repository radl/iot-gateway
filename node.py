from bluepy.btle import *
import threading
import ctypes

from uuid import makeUUID

class SensorNode(threading.Thread):
    """Sensor Node representation

    Connect to a sensor node given its BLE Address, then fetch device information
    and subscribe for notifications.
    
    Every notification is then transfered through the GraphQL API Client.
    """

    class Delegate(DefaultDelegate):
        """Delegate to handle Sensor Node notifications

        All captured notifications are transfered through the GraphQL API Client.
        """

        def __init__(self, serialNumber, handles, api):
            """Constructor for Delegate

            Args:
                serialNumber (str): Serial number of the Sensor Node
                handles (Dict[int, str]): Dictionary of characteristics (handle : UUID)
                api (API): GraphQL API Client
            """
            DefaultDelegate.__init__(self)
            self._serialNumber = serialNumber
            self._handles = handles
            self._api = api

        def log(self, msg):
            """Delegate logger

            Args:
                msg (str): Message to log
            """
            print("BLE> [" + self._serialNumber + "] - " + str(msg))

        def handleNotification(self, cHandle, data):
            """Notification handler

            Handles the notification by mapping the handle to a characteristic UUID,
            then transfer the data through the GraphQL API Client

            Args:
                cHandle (int): Characteristic's handle value
                data (bytes): Data received
            """
            if cHandle in self._handles.keys():
                if self._handles[cHandle] == makeUUID("2a19", False):
                    self._api.updateBatteryLevel(self._serialNumber, int.from_bytes(data, "little", signed=False))

                elif self._handles[cHandle] == makeUUID("ec01", True):
                    self._api.logTemperature(self._serialNumber, int.from_bytes(data, "little", signed=False))

                elif self._handles[cHandle] == makeUUID("ec02", True):
                    self._api.logHumidity(self._serialNumber, int.from_bytes(data, "little", signed=False))

                elif self._handles[cHandle] == makeUUID("ec03", True):
                    self._api.logCO2Level(self._serialNumber, int.from_bytes(data, "little", signed=False))

                elif self._handles[cHandle] == makeUUID("ec04", True):
                    self._api.logLightIntensity(self._serialNumber, int.from_bytes(data, "little", signed=False))

                elif self._handles[cHandle] == makeUUID("ec05", True):
                    self._api.logSoundLevel(self._serialNumber, int.from_bytes(data, "little", signed=False))

                elif self._handles[cHandle] == makeUUID("ec06", True):
                    self._api.logMotionDetector(self._serialNumber, bool.from_bytes(data, "little", signed=False))

                else:
                    self.log("Handle not implemented (" + cHandle + ")")

            else:
                self.log("Handle not supported (" + cHandle + ")")

    def __init__(self, addr, api):
        """Constructor for SensorNode

        Args:
            addr (str): BLE Peripheral Address (e.g. "11:22:33:ab:cd:ed")
            api (API): GraphQL API Client
        """
        threading.Thread.__init__(self)
        self._peripheral = Peripheral() 
        self._event = threading.Event()
        self._addr = addr
        self._api = api

        self._connected = False
        self._handles = {}

        self._serialNumber = None
        self._deviceName = None

    def log(self, msg):
        """SensorNode logger

        Args:
            msg (str): Message to log
        """
        print("BLE> [" + (self._addr if self._serialNumber is None else self._serialNumber) + "] - " + str(msg))

    def _connect(self):
        """Connect to the peripheral (Sensor Node).

        Tries to connect to the peripheral until successful.

        Once connected, it will fetch the device informations and subscribe for notifications.

        Raises:
            SystemExit: if not able to connect, it will only exit the infinite loop if `SystemExit` event is thrown
        """
        while True:
            try:
                self.log("Trying to connect...")
                self._peripheral.connect(self._addr)

                # Fetch device information
                self._device_info()
                self._api.connectNode(self._serialNumber, self._deviceName)

                # Enable notifications
                self._enable_notifications()
                self._peripheral.withDelegate(self.Delegate(self._serialNumber, self._handles, self._api))
                
                self._connected = True
                self.log("Connected")
                break

            except SystemExit:
                raise

            except:
                self.log("Failed to connect")
                # Retries connection after 1s delay
                self._event.wait(1.0)

    def _device_info(self):
        """Fetch device information (Serial number, Device name)

        Raises:
            Exception: Raised if fetching the device information fails

        Returns:
            Tuple[str, str]: Pair (Serial number, Device name)
        """
        try:
            if self._serialNumber is None or self._deviceName is None:
                if self._peripheral is not None:
                    for service in self._peripheral.getServices():
                        if str(service.uuid) == makeUUID("ee01", True):
                            for characteristic in service.getCharacteristics():
                                if characteristic.supportsRead():
                                    if self._serialNumber is None and str(characteristic.uuid) == makeUUID("2a25", False):
                                        self._serialNumber = characteristic.read().decode('utf-8')

                                    elif self._deviceName is None and str(characteristic.uuid) == makeUUID("2a00", False):
                                        self._deviceName = characteristic.read().decode('utf-8')

            # Checks whether the information has been retrieved
            assert self._serialNumber is not None and self._deviceName is not None

            return self._serialNumber, self._deviceName
            
        except:
            raise Exception("Could not retrieve device information")

    def _enable_notifications(self):
        """Subscribe for notifications

        Scan all characteristics and enable notification if they support it.

        Register the characteristic handles to be able to identify the notifications.
        """
        if self._peripheral is not None:
            for service in self._peripheral.getServices():
                for characteristic in service.getCharacteristics():
                    handle = characteristic.getHandle()
                    uuid = str(characteristic.uuid)

                    if "NOTIFY" in characteristic.propertiesToString():
                        self._peripheral.writeCharacteristic(handle + 1, b"\x01\x00")
                        self._handles[handle] = uuid

            self.log("Notifications enabled")

    def _disconnect(self):
        """Disconnect from the peripheral and inform the GraphQL API Client
        """
        if self._peripheral is not None:
            try:
                self._peripheral.disconnect()

            except SystemExit:
                raise

            finally:
                if self._serialNumber is not None:
                    self._api.disconnectNode(self._serialNumber)

        self._connected = False

    def get_id(self):
        """Get the current thread id

        Returns:
            int: Current thread id
        """
        for id, thread in threading._active.items(): 
            if thread is self: 
                return id

    def run(self):
        """Code to be ran by the thread

        Connects to the Sensor Node and poll for notifications.

        If the connection fails, disconnects properly before trying the connection again.
        """
        while True:
            try:
                if not self._connected:
                    self._connect()

                self._peripheral.waitForNotifications(1.0)

            # Exit the infinite loop only when system exits
            except SystemExit:
                break

            except:
                self._disconnect()

        self._disconnect()

    def stop(self):
        """Stop the thread by throwing a `SystemExit` event

        Raises:
            SystemExit: Throws a `SystemExit` to stop the thread
        """
        thread_id = self.get_id()
        res = ctypes.pythonapi.PyThreadState_SetAsyncExc(thread_id, ctypes.py_object(SystemExit))

        if res > 1:
            ctypes.pythonapi.PyThreadState_SetAsyncExc(thread_id, 0)
            self.log("Exception raise failure")
