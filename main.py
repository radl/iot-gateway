from node import SensorNode
from graphql import API

ADDR_LIST = [
    "a4:cf:12:86:4a:c2", # RADL-SN-0001
    "24:62:ab:b2:87:66", # RADL-SN-0002
    "a4:cf:12:86:57:d6", # RADL-SN-0003
    "a4:cf:12:86:4b:52", # RADL-SN-0004
    "a4:cf:12:8a:9a:ca" # RADL-SN-0005
]
"""List of Sensor Node BLE Address"""

def main():
    nodes = []
    api = API()

    print("Starting...")

    try:
        # Start a thread for all sensor nodes
        for addr in ADDR_LIST:
            node = SensorNode(addr, api)
            nodes.append(node)
            node.start()

        # Wait for threads completion
        for node in nodes:
            node.join()

    except KeyboardInterrupt:
        print("Exiting...")
        
        # Stop all threads and wait for them to terminate
        for node in nodes:
            node.stop()
            node.join()


main()
