# RADL IoT Gateway

Implementation for the **IoT Gateway** of the **RADL System**, using the following technologies:
- [Python](https://www.python.org/) Language
- [bluepy](https://github.com/IanHarvey/bluepy) python module (BLE)
- [graphqlclient](https://github.com/prisma-labs/python-graphql-client) python module ([GraphQL](https://graphql.org/) Client)

The purpose of this project is to handle the BLE connection to the **Sensor Nodes** and transmit their data to the **System API**.

## Note

This project does not scan for the sensor nodes, their BLE address is hard-coded.

Next step of the project would be to implement a scanner.

## Getting Started

#### Requirements

This project was implemented with the following:
- [Python](https://www.python.org/) 3.7.3

It is advised to have the same setup to run this project.

#### Usage

The following commands are entered on a terminal with PlatformIO command line tools.

To setup the project:
```shell script
[sudo] make init
```
To run the project:
```shell script
sudo python3 main.py
```

## Bluetooth Architecture

### Architecture

The program handles 3 types of services:
- *0xEC01* - **Device information**:
    - *0x2A00* - Device name (read)
    - *0x2A25* - Serial number (read)
    - *0x2A19* - Battery Level (read | notify)
- *0xEC02* - **Environment reading** (slow update):
    - *0xECFF* - Update frequency (read | write)
    - *0xECxx* - Reading sensor (read | notify)
    - ...
- *0xEC03* - **Environment reading** (fast update):
    - *0xECFF* - Update frequency (read | write)
    - *0xECxx* - Reading sensor (read | notify)
    - ...
- *0xEC04* - **Environment event**:
    - *0xECxx* - Event sensor (read | notify)
    - ...

It makes sense that only one instance of `Device Information` is allowed.

Both `Environment reading` and `Environment event` services can be instanciated multiple times, as well as contain unlimited amound of sensors. All sensors should be identifiable via their UUID, following the format `0xECxx`.

For `Environment reading`, the `Update frequency` characteristic is mandatory.

### UUIDs

Bluetooth specifications propose a list of UUIDs for defined characteristics.
These standard UUIDs are used here for `Device Name`, `Serial Number` and `Battery Level`.
All the other UUIDs used for this project are custom.

The Bluetooth 128 bits UUIDs can be reduced to a 16 bits ID, incorporated into a base 128 bits UUID. The same principle is used to define the custom UUIDs for this project:

|   Type   |               Base UUID              |
|:--------:|:------------------------------------:|
| STANDARD | 0000xxxx-0000-1000-8000-00805F9B34FB |
| CUSTOM   | 0000xxxx-9DED-11EA-AB12-0800200C9A66 |

The following is the list of 16 bits ID already defined for this project:

| 16 bits ID |      Sensor     |   Type  |   MIN   |   MAX  | Unit |
|:----------:|:---------------:|:-------:|:-------:|:------:|:----:|
|   0xEC01   |   Temperature   | Reading |    0    |   100  |  °C  |
|   0xEC02   |     Humitidy    | Reading |    0    |   100  |   %  |
|   0xEC03   |    C02 Level    | Reading |    0    |  65535 |  PPM |
|   0xEC04   | Light Intensity | Reading |    0    |  65535 |  Lux |
|   0xEC05   |   Sound Level   | Reading |    0    |   100  |  dB  |
|   0xEC06   | Motion Detector |  Event  | `false` | `true` |   -  |
