def makeUUID(id, custom):
    """Creates a 128 bits UUID from 16 bits id

    |   Type   |               Base UUID              |
    |:--------:|:------------------------------------:|
    | STANDARD | 0000xxxx-0000-1000-8000-00805f9b34fb |
    | CUSTOM   | 0000xxxx-9ded-11ea-ab12-0800200c9a66 |

    Args:
        id (str): 16 bits id
        custom (bool): whether the UUID is custom or standard

    Returns:
        str: 128 bits UUID
    """
    return "0000" + id + ("-9ded-11ea-ab12-0800200c9a66" if custom else "-0000-1000-8000-00805f9b34fb")
